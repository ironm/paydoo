General
---

In terms of security, I have closed the API only to specified IPs that can access without authentication.
I.e. the internal clients' servers, specified in the `config/packages/securirty.yaml` file.
We can further extend this to require authentication in order to be able to use the API, and then allow external clients to access the API.

In regard to weather stations being located in different cities, I have set up the schema in a way that it allows for a station to be located in a city.
We can then modify our API to allow for a city specification that can read only stations located in this city.

In terms of units, I have decided to keep it simple and store everything in the metric system, instead of allowing the possibility to store in different units.
This means that I will convert the data received in imperial system and then (if needed) convert it back for display.
I am aware of the possibility for rounding issues, but since it is not specified I decided to go with this solution.

Migration
---

There is a migration file under `migrations` that displays the database schema used in the application.

Command
---

File `src/Command/Station/InsertReportCommand.php` acts as a processor of the weather station data that is coming in.
The idea is to setup this command as a cron job and run it once per day to process the incoming data.

Controller
---

File `src/Controller/StationsController.php` acts as a main entry for the API.
It contains three API routes that cover the specified cases.

Entity and Repository
---

We have the Doctrine entities and repositories stored here, used for the application.

Factory and Service
---

These are classes used for decoupling the code and staying in line with the SOLID principles.
