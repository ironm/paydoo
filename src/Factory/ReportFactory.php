<?php

namespace App\Factory;

use App\Entity\Report;

class ReportFactory
{
    /**
     * @param array $result
     * @return Report
     * @throws \Exception
     */
    public function fromArray(array $result): Report
    {
        // TODO: Validate $result content and the presense of the keys before using it

        $report = new Report();

        return $report
            ->setTime(new \DateTime($result['time']))
            ->setTemperature($result['temperature'])
            ->setHumidity($result['humidity'])
            ->setRain($result['rain'])
            ->setWind($result['wind'])
            ->setLight($result['light'])
            ->setBatteryLevel($result['battery_level'])
        ;
    }

    /**
     * @param string $extension
     * @return $this
     */
    public function fromFile(string $extension): self
    {
        if ($extension == 'json') {
            // TODO: Convert to metric, pay attention to rounding issues
            // TODO: Convert battery level percentage to an enum value
        } else {
            // TODO: Keep the values as they are, take care of date format
        }

        return $this;
    }
}
