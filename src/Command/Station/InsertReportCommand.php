<?php

namespace App\Command\Station;

use App\Factory\ReportFactory;
use App\Factory\SerializerFactory;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InsertReportCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ReportFactory
     */
    private $reportFactory;

    /**
     * @var \Symfony\Component\Serializer\Serializer
     */
    private $serializer;

    private $logger;

    /**
     * @param EntityManagerInterface $entityManager
     * @param ReportFactory $reportFactory
     * @param SerializerFactory $serializerFactory
     * @param LoggerInterface $logger
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ReportFactory $reportFactory,
        SerializerFactory $serializerFactory,
        LoggerInterface $logger
    )
    {
        $this->entityManager = $entityManager;
        $this->reportFactory = $reportFactory;
        $this->serializer = $serializerFactory->build();
        $this->logger = $logger;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setName('station:report:insert')
            ->setDescription('Inserts station\'s report')
            ->setHelp('This command allows you to insert the data delivered for the weather stations')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        // TODO: [improvement] Stations can be added easily as files or directories, depending on business rules. We can store stations by directory name, or if everything is in a file we can look for the station name and city.
        foreach (new \DirectoryIterator('data') as $file) {
            if ($file->getExtension() == 'csv' || $file->getExtension() == 'json') {
                // TODO: Decode only files which have today's date

                // Decode the csv and json files to a PHP array
                $results = $this->serializer->decode(
                    file_get_contents($file->getPathname()),
                    $file->getExtension()
                );

                // Build a report object which is normalized to a metric system and store it in database
                foreach ($results as $result) {
                    try {
                        $report = $this->reportFactory->fromFile($file->getExtension())->fromArray($result);

                        $this->entityManager->persist($report);
                        $this->logger->info('Report was successfully saved for file: ' . $file->getPathname());
                    } catch (\Exception $exception) {
                        $this->logger->error('Report was not saved. Error: ' . $exception->getMessage());
                    }
                }
            }
        }

        $this->entityManager->flush();

        return Command::SUCCESS;
    }
}
