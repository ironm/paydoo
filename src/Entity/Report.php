<?php

namespace App\Entity;

use App\Repository\ReportRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ReportRepository::class)
 */
class Report
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $time;

    /**
     * @ORM\Column(type="float")
     */
    private $temperature;

    /**
     * @ORM\Column(type="float")
     */
    private $humidity;

    /**
     * @ORM\Column(type="float")
     */
    private $wind;

    /**
     * @ORM\Column(type="float")
     */
    private $rain;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $light;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('low', 'medium', 'high', 'full')")
     *
     */
    private $batteryLevel;

    /**
     * @ORM\ManyToOne(targetEntity=Station::class, inversedBy="reports")
     * @ORM\JoinColumn(nullable=false)
     */
    private $station;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getTime(): ?\DateTimeInterface
    {
        return $this->time;
    }

    /**
     * @param \DateTimeInterface $time
     * @return $this
     */
    public function setTime(\DateTimeInterface $time): self
    {
        $this->time = $time;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getTemperature(): ?float
    {
        return $this->temperature;
    }

    /**
     * @param float $temperature
     * @return $this
     */
    public function setTemperature(float $temperature): self
    {
        $this->temperature = $temperature;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getHumidity(): ?float
    {
        return $this->humidity;
    }

    /**
     * @param float $humidity
     * @return $this
     */
    public function setHumidity(float $humidity): self
    {
        $this->humidity = $humidity;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getWind(): ?float
    {
        return $this->wind;
    }

    /**
     * @param float $wind
     * @return $this
     */
    public function setWind(float $wind): self
    {
        $this->wind = $wind;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getRain(): ?float
    {
        return $this->rain;
    }

    /**
     * @param float $rain
     * @return $this
     */
    public function setRain(float $rain): self
    {
        $this->rain = $rain;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getLight(): ?float
    {
        return $this->light;
    }

    /**
     * @param float|null $light
     * @return $this
     */
    public function setLight(?float $light): self
    {
        $this->light = $light;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getBatteryLevel(): ?string
    {
        return $this->batteryLevel;
    }

    /**
     * @param string $batteryLevel
     * @return $this
     */
    public function setBatteryLevel(string $batteryLevel): self
    {
        $this->batteryLevel = $batteryLevel;

        return $this;
    }

    /**
     * @return Station|null
     */
    public function getStation(): ?Station
    {
        return $this->station;
    }

    /**
     * @param Station|null $station
     * @return $this
     */
    public function setStation(?Station $station): self
    {
        $this->station = $station;

        return $this;
    }
}
