<?php

namespace App\Controller;

use App\Entity\Station;
use App\Service\Report\AverageInfo;
use App\Service\Report\BasicInfo;
use App\Service\Report\LastInfo;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class StationsController extends AbstractController
{
    /**
     * @Route("/api/stations/all/dates/last", name="stations.last_info")
     *
     * @param LastInfo $service
     * @return JsonResponse
     */
    public function lastInfo(LastInfo $service): JsonResponse
    {
        $data = $service->execute();

        return $this->json($data);
    }

    /**
     * TODO: [improvement] Add city as additional parameter to this API URL in order to select all stations from that particular city.
     *
     * @Route("/api/stations/all/dates/{date}", name="stations.average_info")
     *
     * @param \DateTime $dateTime
     * @param AverageInfo $service
     * @return JsonResponse
     */
    public function averageInfo(\DateTime $dateTime, AverageInfo $service): JsonResponse
    {
        $data = $service->execute($dateTime);

        return $this->json($data);
    }

    /**
     * @Route("/api/stations/{station}/dates/{date}", name="stations.basic_info")
     *
     * @param Station $station
     * @param \DateTime $dateTime
     * @param BasicInfo $service
     * @return JsonResponse
     */
    public function basicInfo(Station $station, \DateTime $dateTime, BasicInfo $service): JsonResponse
    {
        $data = $service->execute($station, $dateTime);

        return $this->json($data);
    }
}
