<?php

namespace App\Service\Report;

use App\Entity\Station;
use App\Repository\ReportRepository;
use Psr\Log\LoggerInterface;

class BasicInfo
{
    /**
     * @var ReportRepository
     */
    private $reportRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param ReportRepository $reportRepository
     * @param LoggerInterface $logger
     */
    public function __construct(ReportRepository $reportRepository, LoggerInterface $logger)
    {
        $this->reportRepository = $reportRepository;
        $this->logger = $logger;
    }

    /**
     * @param Station $station
     * @param \DateTime $dateTime
     * @return array
     */
    public function execute(Station $station, \DateTime $dateTime): array
    {
        $this->logger->info(sprintf("Basic data for station %s requested", $station->getName()));

        $report = $this->reportRepository->findOneBy([
            'station_id' => $station->getId(),
            'time' => $dateTime->format('Y-m-d H:i:s')
        ]);

        $data = [
            'temperature' => $report->getTemperature(),
            'humidity' => $report->getHumidity(),
            'wind' => $report->getWind(),
        ];

        $this->logger->info('Basic data successfully computed.');

        return $data;
    }
}
