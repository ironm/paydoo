<?php

namespace App\Service\Report;

use App\Repository\ReportRepository;
use Doctrine\ORM\NonUniqueResultException;
use Psr\Log\LoggerInterface;

class LastInfo
{
    /**
     * @var ReportRepository
     */
    private $reportRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param ReportRepository $reportRepository
     * @param LoggerInterface $logger
     */
    public function __construct(ReportRepository $reportRepository, LoggerInterface $logger)
    {
        $this->reportRepository = $reportRepository;
        $this->logger = $logger;
    }

    /**
     * @return array
     */
    public function execute(): array
    {
        try {
            $report = $this->reportRepository->findLastReport();
            $this->logger->info('Report found.');
        } catch (NonUniqueResultException $exception) {
            $this->logger->error('Woops, something went wrong');
        }

        $data = [
            'time' => $report->getTime()
        ];

        $this->logger->info('Last report date successfully computed.');

        return $data;
    }
}
