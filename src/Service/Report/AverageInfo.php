<?php

namespace App\Service\Report;

use App\Repository\ReportRepository;
use Psr\Log\LoggerInterface;

class AverageInfo
{
    /**
     * @var ReportRepository
     */
    private $reportRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param ReportRepository $reportRepository
     * @param LoggerInterface $logger
     */
    public function __construct(ReportRepository $reportRepository, LoggerInterface $logger)
    {
        $this->reportRepository = $reportRepository;
        $this->logger = $logger;
    }

    /**
     * @param \DateTimeInterface $dateTime
     * @return array
     */
    public function execute(\DateTimeInterface $dateTime): array
    {
        $this->logger->info('Average data for all stations requested.');

        $reports = $this->reportRepository->findByDate($dateTime);

        // TODO: [improvement] Move the average calculation to the Station entity and get the average per station instead of calculating like this.

        foreach ($reports as $report) {
            // TODO: Do some calculation and get the average
            $averageTemperature = 0;
            $averageHumidity = 0;
            $averageWind = 0;
        }

        $data = [
            'temperature' => $averageTemperature,
            'humidity' => $averageHumidity,
            'wind' => $averageWind,
        ];

        $this->logger->info('Average data successfully computed.');

        return $data;
    }
}
